// for example
#include <iostream>
#include <vector>

// required for smart_lock
#include <mutex>

struct ForwardArgsToCTor_t {};
constexpr ForwardArgsToCTor_t ForwardArgsToCTor{};

template <typename T>
class smart_lock {
   private:
    class Proxy {
       public:
        Proxy(T& base, std::mutex& lock);
        ~Proxy();

        Proxy(const Proxy&) = delete;
        Proxy(Proxy&&) = delete;
        Proxy& operator=(const Proxy&) = delete;
        Proxy& operator=(Proxy&&) = delete;

        T* operator->();
        const T* operator->() const;
        T& operator*();
        const T& operator*() const;

       private:
        T& base;
        std::mutex& lock;
    };

    class ConstProxy {
       public:
        ConstProxy(const T& base, std::mutex& lock);
        ~ConstProxy();

        ConstProxy(const ConstProxy&) = delete;
        ConstProxy(ConstProxy&&) = delete;
        ConstProxy& operator=(const ConstProxy&) = delete;
        ConstProxy& operator=(ConstProxy&&) = delete;

        const T* operator->() const;
        const T& operator*() const;

       private:
        const T& base;
        std::mutex& lock;
    };

   public:
    smart_lock() = default;

    template <typename... Args>
    explicit smart_lock(ForwardArgsToCTor_t, Args&&... args);

    smart_lock(const smart_lock& rhs);
    smart_lock(smart_lock&& rhs);
    smart_lock& operator=(const smart_lock& rhs);
    smart_lock& operator=(smart_lock&& rhs);

    ~smart_lock() = default;

    Proxy operator->();
    ConstProxy operator->() const;

    Proxy get_scope_guard();
    ConstProxy get_scope_guard() const;

    T get_copy() const;

   private:
    T base;
    mutable std::mutex lock;
};

template <typename T>
template <typename... Args>
inline smart_lock<T>::smart_lock(ForwardArgsToCTor_t, Args&&... args)
    : base(std::forward<Args>(args)...) {}

template <typename T>
inline smart_lock<T>::smart_lock(const smart_lock& rhs)
    : base([&] {
          std::lock_guard<std::mutex> guard(rhs.lock);
          return rhs.base;
      }()) {}

template <typename T>
inline smart_lock<T>::smart_lock(smart_lock&& rhs)
    : base([&] {
          std::lock_guard<std::mutex> guard(rhs.lock);
          return std::move(rhs.base);
      }()) {}

template <typename T>
inline smart_lock<T>& smart_lock<T>::operator=(const smart_lock& rhs) {
    if (this != &rhs) {
        std::lock(this->lock, rhs.lock);
        std::lock_guard<std::mutex> guard(this->lock, std::adopt_lock);
        std::lock_guard<std::mutex> guardRhs(rhs.lock, std::adopt_lock);
        this->base = rhs.base;
    }

    return *this;
}

template <typename T>
inline smart_lock<T>& smart_lock<T>::operator=(smart_lock&& rhs) {
    if (this != &rhs) {
        std::lock(this->lock, rhs.lock);
        std::lock_guard<std::mutex> guard(this->lock, std::adopt_lock);
        std::lock_guard<std::mutex> guardRhs(rhs.lock, std::adopt_lock);
        this->base = std::move(rhs.base);
    }
    return *this;
}

template <typename T>
inline typename smart_lock<T>::Proxy smart_lock<T>::operator->() {
    return Proxy(this->base, this->lock);
}

template <typename T>
inline typename smart_lock<T>::ConstProxy smart_lock<T>::operator->() const {
    return ConstProxy(this->base, this->lock);
}

template <typename T>
inline typename smart_lock<T>::Proxy smart_lock<T>::get_scope_guard() {
    return Proxy(this->base, this->lock);
}

template <typename T>
inline typename smart_lock<T>::ConstProxy smart_lock<T>::get_scope_guard()
    const {
    return ConstProxy(this->base, this->lock);
}

template <typename T>
inline T smart_lock<T>::get_copy() const {
    std::lock_guard<std::mutex> guard(this->lock);
    return this->base;
}

// PROXY OBJECT

template <typename T>
inline smart_lock<T>::Proxy::Proxy(T& base, std::mutex& lock)
    : base(base), lock(lock) {
    this->lock.lock();
}

template <typename T>
inline smart_lock<T>::Proxy::~Proxy() {
    this->lock.unlock();
}

template <typename T>
inline T* smart_lock<T>::Proxy::operator->() {
    return &this->base;
}

template <typename T>
inline const T* smart_lock<T>::Proxy::operator->() const {
    return const_cast<smart_lock<T>::Proxy*>(this)->operator->();
}

template <typename T>
inline T& smart_lock<T>::Proxy::operator*() {
    return this->base;
}

template <typename T>
inline const T& smart_lock<T>::Proxy::operator*() const {
    return this->base;
}

template <typename T>
inline smart_lock<T>::ConstProxy::ConstProxy(const T& base, std::mutex& lock)
    : base(base), lock(lock) {
    this->lock.lock();
}

template <typename T>
inline smart_lock<T>::ConstProxy::~ConstProxy() {
    this->lock.unlock();
}

template <typename T>
inline const T* smart_lock<T>::ConstProxy::operator->() const {
    return &this->base;
}

template <typename T>
inline const T& smart_lock<T>::ConstProxy::operator*() const {
    return this->base;
}

template <typename T>
using ThreadSafeVector = smart_lock<std::vector<T>>;

int main() {
    ThreadSafeVector<int> vec;
    vec->emplace_back(123);
    vec->emplace_back(456);

    {
        auto guard = vec.get_scope_guard();
        for (auto& e : *guard) {
            std::cout << e << std::endl;
        }
    }
}
