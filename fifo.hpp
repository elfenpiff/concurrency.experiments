#include <array>
#include <atomic>

// guarantee is
//      no data loss
//      data is in the right order
namespace LockFree
{
// TODO: see sofi3.hpp after FiFo_weak
template < typename Type, size_t Size >
class FiFo_weak
{
  public:
    FiFo_weak()
    {
        std::fill(storage.begin(), storage.end(), nullptr);
    }

    // weak, it is possible that push returns false if the fifo has space left
    bool push(const Type& in)
    {
        Type expected     = nullptr;
        auto l_writeIndex = writeIndex.load();
        // TODO: for demonstration purposes:
        // auto a = readIndex.load(); // massive cache sync reduce performance
        // TODO: end for demonstration purposes:
        if (storage[l_writeIndex % Size].compare_exchange_weak(expected, in))
        {
            // if another thread performs an exchange when this thread
            // is interrupted here, it returns false
            writeIndex.store(l_writeIndex + 1);
            return true;
        }

        return false;
    }

    // weak, if the fifo has elements it is possible that false is returned
    bool pop(Type& out)
    {
        auto l_readIndex = readIndex.load();
        out              = storage[l_readIndex % Size].exchange(nullptr);
        if (out != nullptr)
        {
            // if another thread performs an exchange when this thread
            // is interrupted here, it returns false
            readIndex.store(l_readIndex + 1);
            return true;
        }

        return false;
    }

    bool isEmpty()
    {
        return writeIndex.load() <= readIndex.load();
    }

  private:
    static constexpr int  PageSize = 64;
    std::atomic< size_t > readIndex{0};
    //   char                                    pad1[PageSize]; // better cache line alignment
    std::atomic< size_t > writeIndex{0};
    //   char                                    pad2[PageSize]; // better cache line alignment
    std::array< std::atomic< Type >, Size > storage;
};
} // namespace LockFree
